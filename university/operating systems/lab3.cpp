// ZubtsovAslanov627var1lab3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"
#define WINAPISTYLE


int findmax(int *arr, int N, int M)
{
	int max=0;
	for(int i=1; i<N*M; i++)
		if(*(arr+i)>*(arr+max))
			max=i;
	return max;
}

int findmin(int *arr, int N, int M)
{
	int min=0;
	for(int i=1; i<N*M; i++)
		if(*(arr+i)<*(arr+min))
			min=i;
	return min;
}

void input (int *arr, int N, int M)
{
	for (int i=0; i<N*M; i++)
			scanf("%d",&arr[i]);
}


int _tmain(int argc, _TCHAR* argv[])
{
	int N,M;
	char c;
	int *A;
	HANDLE hHeap;			// ��������� �� ����
	do
	{
		printf("input N, M: ");
		scanf("%d %d", &N, &M);
		if (N>0 && M>0) 
		{	

#ifndef WINAPISTYLE
			int *A = new int[N*M];
#endif

#ifdef WINAPISTYLE
			//hHeap=HeapCreate(0,0x01000,0);	// �������
			hHeap=HeapCreate(0,sizeof(int)*N*M,0);	// �������

			if (hHeap!=NULL)			// ���� ������� ������
			{
				A=(int*)HeapAlloc(hHeap,0,sizeof(int)*N*M);	// �������� � ����� ������ �����
				if(!A)
				{
					printf("Again...\n");
					continue;
				}
			}
			else
			{
				printf("Error Create Heap\n");
				continue;
			}
#endif				
			printf("input array (by string):\n");
			input(A, N, M);
			printf("max=%d, min=%d\n", A[findmax(A, N, M)], A[findmin(A,N,M)]);		

#ifndef WINAPISTYLE
			delete [] A;
#endif

#ifdef WINAPISTYLE
			// ��������� � �������� � ������ ������
			if (HeapFree(hHeap,0,A)==0)
				printf("Error Free\n");
			// ��������� � �������� � ������ ������
			if (HeapDestroy(hHeap)==0)
				printf("Error delete Heap\n");
#endif
		}
		else printf("NO WAY!\n");
		printf("Again? Y/N: ");
		scanf("\n%c", &c);
		while(getchar()!='\n');
	} while(c!='N');
	return 0;
}

