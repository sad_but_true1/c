// ZubtsovAslanov627var1lab12.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>

void input (int *arr, int n, int m)
{
	for (int i=0; i<n*m; i++)
			scanf("%d",&arr[i]);
}

void find (int *arr, int n, int m)
{
	for (int i=0; i<n*m; i++)
		for(int j=i+1; j<m*n; j++)
			if(*(arr+i)==*(arr+j))
			{
				printf("A[%d,%d]==A[%d,%d];\n",i/m,i%m,j/m,j%m);
				return;
			}
}

int _tmain(int argc, _TCHAR* argv[])
{
	int A[2*10];
	printf("input array (by string):\n");
	input(A,2,10);
	for (int i=0; i<2; i++)
	{
		for (int j=0; j<10; j++)
			printf("%d ",A[i*10+j]);
		printf("\n");
	}
	find(A, 2, 10);
	getchar();
	getchar();
	return 0;
}

