#include <windows.h>
#include <iostream>

using namespace std;

struct Map
{
	int key, value;
	HANDLE hHeap;
};

Map *mapCreate()
{
	HANDLE hHeap = HeapCreate(0, sizeof(Map), 0);
	Map *X = (Map *) HeapAlloc(hHeap, HEAP_ZERO_MEMORY, sizeof(Map));
	X->hHeap=hHeap;
	return X;
}

void mapAdd(Map *X, int key, int value)
{
	DWORD size = HeapSize(X->hHeap, 0, X);
	X[(size/sizeof(Map))-1].key = key;
	X[(size/sizeof(Map))-1].value = value;
	X[(size/sizeof(Map))-1].hHeap = X->hHeap;
	X = (Map *) HeapReAlloc(X->hHeap, HEAP_ZERO_MEMORY, X, size + sizeof(Map));
}

int mapFind(Map *X, int key)
{
	DWORD size = HeapSize(X->hHeap, 0, X);
	for (int i = 0; i < (size/sizeof(Map)) - 1; i++)
		if (X[i].key == key) return i;
	return -1;
}

bool mapElementDelete(Map *X, int key)
{
	DWORD size = HeapSize(X->hHeap, 0, X);
	if (size == sizeof(Map))
		return 0;
	for (int i = 0; i < (size/sizeof(Map)) - 1; i++)
		if (X[i].key == key)
		{
			X[i].key = X[(size/sizeof(Map)) - 2].key;
			X[i].value = X[(size/sizeof(Map)) - 2].value;
			X[i].hHeap = X[(size/sizeof(Map)) - 2].hHeap;
			HANDLE hHeap = X[(size/sizeof(Map)) - 2].hHeap;
			memset(&X[size/sizeof(Map) - 2], 0, sizeof(Map));
			X[size/sizeof(Map) - 2].hHeap = hHeap;
			X = (Map *) HeapReAlloc(X->hHeap, 0, X, size - sizeof(Map));
			return 1;
		}
	return 0;
}

void mapOutput(Map *X)
{
	DWORD size = HeapSize(X->hHeap, 0, X);
	if (size == sizeof(Map))
		cout << "Empty map." <<endl;
	else
		for (int i = 0; i < (size/sizeof(Map)) - 1; i++)
			cout << "Key: " << X[i].key << " Value: " << X[i].value << endl;
}

bool mapDelete(Map *X)
{
	return HeapDestroy(X->hHeap);
}