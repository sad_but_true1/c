// ZubtsovAslanov627var1lab4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "map.h"
#include <iostream>

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	int key, value;
	bool exit = 0;
	Map *testMap = mapCreate();
	do
	{
		cout << "Menu:\n1. Add elemet to map.\n2. Delete element from map.\n3. Output map element by key.\n4. Output map.\n5. Save map.\n6. Load map from file.\n7. Exit.\nType number: ";
		short choise;
		cin >> choise;
		switch(choise)
		{
		case 1:
			{
				cout << "Key: ";
				cin >> key;
				cout << "Value: ";
				cin >> value;
				mapAdd(testMap, key, value);
			};break;
		case 2:
			{
				if (HeapSize(testMap->hHeap, 0, testMap) == sizeof(Map))
					cout << "Empty map." <<endl;
				else
				{
					cout << "Key: ";
					cin >> key;
					mapElementDelete(testMap, key);
				}
			};break;
		case 3:
			{
				if (HeapSize(testMap->hHeap, 0, testMap) == sizeof(Map))
					cout << "Empty map." <<endl;
				else
				{
					cout << "Key: ";
					cin >> key;
					cout << "Key: " << testMap[mapFind(testMap, key)].key << " Value: " << testMap[mapFind(testMap, key)].value <<endl;
				}
			};break;
		case 4:
			{
				mapOutput(testMap);
			};break;
		case 5:
			{
				cout << "Input filename: ";
				char filename[200];
				cin >> filename;
				mapSave(testMap, filename);
			};break;
		case 6:
			{
				cout << "Input filename: ";
				char filename[200];
				cin >> filename;
				if (mapDelete(testMap) == 0) cout << "Error while destroing HEAP!";
				testMap = mapLoad(filename);
			};break;
		case 7:
			{
				exit = 1;
			};break;
		}
	} while(!exit);
	if (mapDelete(testMap) == 0) cout << "Error while destroing HEAP!";
	system("PAUSE");
	return 0;
}