#include <iostream>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

enum State {S, Ai, Ac, As, C, B, E, F};
enum LexType {lIf, lThen, lElse, lElseif, lEnd, lAnd, lOr, lInput, lOutput, lRel, lAo, lEqual, lUnequal, lAfo, lAfc, lRaz, lConst, lVar};
enum EEntryType {etCmd, etVar, etConst, etCmdPtr};
enum ECmd {JMP, JZ, SET, ADD, SUB, MUL, DIV, AND, OR, CMPE, CMPNE, CMPL, CMPG, OUTPUT, INPUT};
static const char lexRel[]={'<','>'};
static const char lexAo[]={'+','-','*','/'};
int PostFixSize=0;

using namespace std;

struct Lex
{
    LexType type;
    int index,relpos;
    int posBegin,lenght;
    Lex *next,*prev;
    Lex (LexType _type,int _index, int begin,int _lenght,int pos): type(_type),index(_index),relpos(pos),posBegin(begin),lenght(_lenght){}
}*pLexListFirst=NULL,*pLexListLast=NULL,*pCurrent=NULL;

struct ConstList
{
    int count,number;
    ConstList *next,*prev;
    ConstList (int _count, int _number):count(_count),number(_number){}
}*pConstFirst=NULL,*pConstLast=NULL;

struct Var
{
    int number,count;
    const char *begin,*end;
    Var *next,*prev;
    Var (int _count, const char *lexstart, const char *str): number(_count), begin(lexstart), end(str){}
}*pVarFirst=NULL,*pVarLast=NULL;

struct PostFixEntry {
	EEntryType type;
	int index,count;
	PostFixEntry *next,*prev;
}*pPostFixCurrent=NULL,*pPostFixFirst=NULL,*pPostFixLast=NULL;

PostFixEntry *CreatePostFix(PostFixEntry *head, EEntryType type)
{
    PostFixEntry *p=new PostFixEntry();
    p->type=type;
    p->next=pPostFixLast;
    if (pPostFixLast) pPostFixLast->prev=p;
    pPostFixLast=p;
    if (!pPostFixFirst)
    {
        pPostFixFirst=pPostFixLast;
        pPostFixFirst->count=0;
    }
    else p->count=p->next->count+1;
    return p;
}

int WriteCmd (ECmd cmd)
{
    PostFixEntry *p=CreatePostFix(pPostFixLast, etCmd);
    p->index=cmd;
    PostFixSize++;
    return p->count;
}

int WriteVar(int ind)
{
    PostFixEntry *p=CreatePostFix(pPostFixFirst, etVar);
    p->index=ind;
    PostFixSize++;
    return p->count;
}

int WriteConst(int ind)
{
    PostFixEntry *p=CreatePostFix(pPostFixFirst, etConst);
    p->index=ind;
    PostFixSize++;
    return p->count;
}

int WriteCmdPtr(int ptr)
{
    PostFixEntry *p=CreatePostFix(pPostFixFirst, etCmdPtr);
    p->index=ptr;
    PostFixSize++;
    return p->count;
}

void SetCmdPtr(int ind, int ptr)
{
    PostFixEntry *p=pPostFixFirst;
    int pos;
    while (p && p->count!=ind)
    {
        p=p->prev;
        pos++;
    }
    if (p) p->index=ptr;
}

ConstList *AddConst(ConstList *head, int count, int index)
{
    ConstList *p=new ConstList(count, index);
    p->next=head;
    if (head) head->prev=p;
    return p;
}

Var *AddVar(Var *head, int count, const char *lexstart, const char *str)
{
    Var *p=new Var(count, lexstart, str);
    p->next=head;
    if (head) head->prev=p;
    return p;
}

ConstList *FindConstCount(int count)
{
    ConstList *p=pConstFirst;
    while (p)
    {
        if (p->count==count) return p;
        p=p->prev;
    }
    return p;
}

Var *FindVarCount(const char *lexstart, const char *str)
{
    Var *p=pVarFirst;
    int lenght=str-lexstart;
    while(p)
    {
        if (lenght==p->end-p->begin)
        {
            bool equal=true;
            for (int i=0; i<lenght; i++)
            if (lexstart[i]!=(p->begin)[i])
            {
                equal=false;
                break;
            }
            if (equal) return p;
        }
        p=p->prev;
    }
    return p;
}

int PosOfRelArith(char *txt)
{
    if (strcmp(txt,"+")==0) return 1;
    else if (strcmp(txt,"-")==0) return 2;
    else if (strcmp(txt,"*")==0) return 3;
    else if (strcmp(txt,"/")==0) return 4;
    else if (strcmp(txt,"<>")==0) return 5;
    else if (strcmp(txt,"=")==0) return 6;
    else if (strcmp(txt,"<")==0) return 7;
    else if (strcmp(txt,">")==0) return 8;
    else return -1;
}

Lex* AddLex(Lex* head, State cstate, const char *text, const char *lexstart, const char *str)
{
    LexType type;
    int index;
    if (cstate==Ai)
    {
        int i=0,lenght=str-lexstart;
        char *c=new char[lenght+1];
        const char *begin=lexstart;
        while (begin!=str)
        {
            c[i]=*begin;
            begin++;
            i++;
        }
        c[i]=0;
        bool added=false;
        if (lenght==2)
        {
            if (strcmp(c,"if")==0)
            {
                type=lIf;
                index=0;
                added=true;
            }
            else if (strcmp(c,"or")==0)
            {
                type=lOr;
                index=0;
                added=true;
            }
        }
        else if (lenght==3)
        {
            if (strcmp(c,"end")==0)
            {
                type=lEnd;
                index=0;
                added=true;
            }
            else if (strcmp(c,"and")==0)
            {
                type=lAnd;
                index=0;
                added=true;
            }
        }
        else if (lenght==4)
        {
            if (strcmp(c,"else")==0)
            {
                type=lElse;
                index=0;
                added=true;
            }
            else if (strcmp(c,"then")==0)
            {
                type=lThen;
                index=0;
                added=true;
            }
        }
        else if (lenght==5)
        {
            if (strcmp(c,"input")==0)
            {
                type=lInput;
                index=0;
                added=true;
            }
        }
        else if (lenght==6)
        {
            if (strcmp(c,"output")==0)
            {
                type=lOutput;
                index=0;
                added=true;
            }
            else if (strcmp(c,"elseif")==0)
            {
                type=lElseif;
                index=0;
                added=true;
            }
        }
        if (!added)
        {
            if (!pVarFirst)
            {
                index=0;
                pVarLast=AddVar(pVarLast, index, lexstart, str);
                pVarFirst=pVarLast;
                type=lVar;
            }
            else
            {
                Var *p=FindVarCount(lexstart,str);
                if (p)
                {
                    index=p->number;
                    type=lVar;
                }
                else
                {
                    index=pVarLast->number+1;
                    pVarLast=AddVar(pVarLast, index, lexstart, str);
                    type=lVar;
                }
            }
        }
        delete c;
        c=NULL;
    }
    else if (cstate==Ac)
    {
        int count=0,i=1;
        while (lexstart!=str)
        {
            count+=(*(str-1)-48)*pow(10,i-1);
            i++;
            str--;
        }
        if (!pConstFirst)
        {
            index=0;
            pConstLast=AddConst(pConstLast, count, index);
            pConstFirst=pConstLast;
            type=lConst;
        }
        else
        {
            ConstList *p=FindConstCount(count);
            if (p)
            {
                index=p->number;
                type=lConst;
            }
            else
            {
                index=pConstLast->number+1;
                pConstLast=AddConst(pConstLast, count, index);
                type=lConst;
            }
        }
    }
    else if (cstate==As)
    {
        if ((str-lexstart==2) && (*(str-1)=='>'))
        {
            type=lUnequal;
            index=0;
        }
        else
        {
            type=lRel;
            index=0;
        }
    }
    else if ((cstate==B) || (cstate==C))
    {
        for (int i=0; i<2; i++) if (*lexstart==lexRel[i])
        {
            type=lRel;
            index=i;
        }
        for (int i=0; i<4; i++) if (*lexstart==lexAo[i])
        {
            type=lAo;
            index=i;
        }
        if (*lexstart=='=')
        {
            type=lEqual;
            index=0;
        }
        if (*lexstart=='(')
        {
            type=lAfo;
            index=0;
        }
        if (*lexstart==')')
        {
            type=lAfc;
            index=0;
        }
        if (*lexstart==';')
        {
            type=lRaz;
            index=0;
        }
    }
    int pos=0;
    if (type==lRel || type==lEqual || type==lUnequal || type==lAo)
    {
        char cLex[10];
        strncpy(cLex, lexstart, str-lexstart);
        pos=PosOfRelArith(cLex);
    }
    Lex* p=new Lex(type,index,str-text-(str-lexstart)+1,str-lexstart,pos);
    p->next=head;
    if (head) head->prev=p;
    return p;
}

ConstList *FindConstIndex(int index)
{
    ConstList *p=pConstFirst;
    while (p && p->number!=index)
    {
        p=p->prev;
    }
    return p;
}

Var *FindVarIndex(int index)
{
    Var *p=pVarFirst;
    while (p && p->number!=index)
    {
        p=p->prev;
    }
    return p;
}

void DeleteLex(Lex *head)
{
    if (head)
    {
        Lex *p;
        while (head)
        {
            p=head;
            head=head->prev;
            delete p;
        }
        pLexListFirst=NULL;
        pLexListLast=NULL;
    }
}

void DeleteConstList(ConstList *head)
{
    if (head)
    {
        ConstList *p;
        while (head)
        {
            p=head;
            head=head->prev;
            delete p;
        }
        pConstFirst=NULL;
        pConstLast=NULL;
    }
}

void DeleteVar(Var *head)
{
    if (head)
    {
        Var *p;
        while (head)
        {
            p=head;
            head=head->prev;
            delete p;
        }
        pVarFirst=NULL;
        pVarLast=NULL;
    }
}

void DeletePostFix(PostFixEntry *head)
{
    if (head)
    {
        PostFixEntry *p;
        while (head)
        {
            p=head;
            head=head->prev;
            delete p;
        }
        pPostFixFirst=NULL;
        pPostFixLast=NULL;
    }
}

void PrintVar(const char *lexstart, const char *str)
{
    while(str!=lexstart)
    {
        cout << *lexstart;
        lexstart++;
    }
}

void OutputConst(ConstList *head)
{
    ConstList *p=head;
    int i=0;
    cout << "Таблица констант:" << endl;
    if (!head) cout << "Таблица пуста" << endl;
    while(p)
    {
        cout << i << " элемент =" << p->count << endl;
        p=p->prev;
        i++;
    }
}

void OutputVar(Var *head)
{
    Var *p=head;
    int i=0;
    cout << "Таблица переменных:" << endl;
    if (!head) cout << "Таблица пуста" << endl;
    while(p)
    {
        cout << i << " элемент ";
        PrintVar(p->begin, p->end);
        cout << '=' << p->count << endl;
        p=p->prev;
        i++;
    }
}

void OutputLex(Lex* head)
{
    Lex* p=head;
    if (!head) cout << "Таблица пуста" << endl;
    while(p)
    {
        switch (p->type)
        {
            case lConst:
            {
                ConstList *pconst=FindConstIndex(p->index);
                cout << "Константа = " << pconst->count << " на позиции " << p->posBegin << " под номером " << pconst->number << " в таблице констант." << endl;
            };break;
            case lVar:
            {
                cout << "Идентификатор ";
                Var *pvar=FindVarIndex(p->index);
                PrintVar(pvar->begin, pvar->end);
                cout << " на позиции " << p->posBegin << " под номером " << p->index << " в таблице переменных." << endl;
            };break;
            case lIf:
            {
                cout << "Ключевое слово IF на позиции " << p->posBegin << '.' << endl;
            };break;
            case lThen:
            {
                cout << "Ключевое слово THEN на позиции " << p->posBegin << '.' << endl;
            };break;
            case lElse:
            {
                cout << "Ключевое слово ELSE на позиции " << p->posBegin << '.' << endl;
            };break;
            case lElseif:
            {
                cout << "Ключевое слово ELSEIF на позиции " << p->posBegin << '.' << endl;
            };break;
            case lEnd:
            {
                cout << "Ключевое слово END на позиции " << p->posBegin << '.' << endl;
            };break;
            case lAnd:
            {
                cout << "Ключевое слово AND на позиции " << p->posBegin << '.' << endl;
            };break;
            case lOr:
            {
                cout << "Ключевое слово OR на позиции " << p->posBegin << '.' << endl;
            };break;
            case lInput:
            {
                cout << "Ключевое слово INPUT на позиции " << p->posBegin << '.' << endl;
            };break;
            case lOutput:
            {
                cout << "Ключевое слово OUTPUT на позиции " << p->posBegin << '.' << endl;
            };break;
            case lRel:
            {
                cout << "Операция сравнения " << lexRel[p->index] << " на позиции " << p->posBegin << " под номером " << p->index << " в таблице операций сравнения." << endl;
            };break;
            case lAo:
            {
                cout << "Арифметическая операция " << lexAo[p->index] << " на позиции " << p->posBegin << " под номером " << p->index << " в таблице арифметических операций." << endl;
            };break;
            case lEqual:
            {
                cout << "Арифметическая операция или операция сравнения = на позиции " << p->posBegin << '.' << endl;
            };break;
            case lUnequal:
            {
                cout << "Операция сравнения <> на позиции " << p->posBegin << '.' << endl;
            };break;
            case lAfo:
            {
                cout << "Открывающаяся скобка ( на позиции " << p->posBegin << '.' << endl;
            };break;
            case lAfc:
            {
                cout << "Закрывающаяся скобка ) на позиции " << p->posBegin << '.' << endl;
            };break;
            case lRaz:
            {
                cout << "Разделитель ; на позиции " << p->posBegin << '.' << endl;
            };break;
        }
        p=p->prev;
    }
}

void OutputPostfix(PostFixEntry *head)
{
    PostFixEntry *p=head;
    while (p)
    {
        cout << p->count << ' ';
        switch (p->type)
        {
            case etConst:
            {
                ConstList *c;
                c=FindConstIndex(p->index);
                if (c) cout << c->count << endl;
            };break;
            case etVar:
            {
                Var *v=FindVarIndex(p->index);
                PrintVar(v->begin, v->end);
                cout << endl;
            };break;
            case etCmd:
            {
                switch (p->index)
                {
                    case SET:cout << "= set" << endl;break;
                    case ADD:cout << '+' << endl;break;
                    case SUB:cout << '-' << endl;break;
                    case MUL:cout << '*' << endl;break;
                    case DIV:cout << '/' << endl;break;
                    case CMPE:cout << '=' << endl;break;
                    case CMPNE:cout << "<>" << endl;break;
                    case CMPG:cout << '>' << endl;break;
                    case CMPL:cout << '<' << endl;break;
                    case JZ:cout <<"JZ" << endl;break;
                    case JMP:cout << "JMP" << endl;break;
                    case OUTPUT:cout << "output" << endl;break;
                    case INPUT:cout << "input" << endl;break;
                    case OR:cout << "or" << endl;break;
                    case AND:cout << "and" << endl;break;
                    default:cout <<"nothing";break;
                }
            }break;
            case etCmdPtr:cout << "pointer to " << p->index << endl;break;
            default:break;
        }
        p=p->prev;
    }
}

bool LexAnalys (const char* text)
{
    const char *str=text,*lexstart=str;
    State state=S,prevState;
    bool add;
    while ((state!=E) && (state!=F) && (*str!=0))
    {
        prevState=state;
        add=true;
        switch(state)
        {
            case S:
            {
                if (isspace(*str));
                else if (isalpha(*str)) state=Ai;
                else if (isdigit(*str)) state=Ac;
                else if ((*str=='=') || (*str=='+') || (*str=='*') || (*str=='/') || (*str=='-') || (*str=='(') || (*str==')') || (*str==';')) state=B;
                else if (*str=='<') state=As;
                else if (*str=='>') state=C;
                else if (*str==0) state=F;
                else state=E;
                add=false;
            }break;
            case Ai:
            {
                if (isspace(*str)) state=S;
                else if (isalnum(*str)) add=false;
                else if ((*str=='=') || (*str=='+') || (*str=='*') || (*str=='/') || (*str=='-') || (*str=='(') || (*str==')') || (*str==';')) state=B;
                else if (*str=='<') state=As;
                else if (*str=='>') state=C;
                else if (*str==0) state=F;
                else
                {
                    state=E;
                    add=false;
                }
            }break;
            case Ac:
            {
                if (isspace(*str)) state=S;
                else if (isalpha(*str)) state=Ai;
                else if (isdigit(*str)) add=false;
                else if ((*str=='=') || (*str=='+') || (*str=='*') || (*str=='/') || (*str=='-') || (*str=='(') || (*str==')') || (*str==';')) state=B;
                else if (*str=='<') state=As;
                else if (*str=='>') state=C;
                else if (*str==0) state=F;
                else
                {
                    state=E;
                    add=false;
                }
            }break;
            case B:
            {
                if (isspace(*str)) state=S;
                else if (isalpha(*str)) state=Ai;
                else if (isdigit(*str)) state=Ac;
                else if (*str=='<')
                {
                    add=false;
                    state=As;
                }
                else if (*str=='>') state=C;
                else if (*str==0) state=F;
                else
                {
                    state=E;
                    add=false;
                }
            }break;
            case As:
            {
                if (isspace(*str)) state=S;
                else if (isalpha(*str)) state=Ai;
                else if (isdigit(*str)) state=Ac;
                else if ((*str=='=') || (*str=='+') || (*str=='*') || (*str=='/') || (*str=='-') || (*str=='(') || (*str==')') || (*str==';')) state=B;
                else if (*str=='>') state=S;
                else if (*str=='<') state=As;
                else if (*str==0) state=F;
                else
                {
                    state=E;
                    add=false;
                }
            }break;
            case C:
            {
                if (isspace(*str)) state=S;
                else if (isalpha(*str)) state=Ai;
                else if (isdigit(*str)) state=Ac;
                else if ((*str=='=') || (*str=='+') || (*str=='*') || (*str=='/') || (*str=='-') || (*str=='(') || (*str==')') || (*str==';')) state=B;
                else if (*str=='<') state=As;
                else if (*str=='>') state=C;
                else if (*str==0) state=F;
                else
                {
                    state=E;
                    add=false;
                }
            }break;
            case E:break;
            case F:break;
        }
        if (add)
        {
            pLexListLast=AddLex(pLexListLast, prevState, text, lexstart, str);
            if (!pLexListFirst) pLexListFirst=pLexListLast;
        }
        if ((state!=prevState) && (state==Ai || state==Ac || state==As || state==B || state==C)) lexstart=str;
        if ((state!=E)&&(state!=F)) str++;
    }
    return (state==F);
}

void Error (const char* msg, int pos)
{
    cout << msg << " на позиции " << pos << '.' << endl;
}

bool IfStatement (bool internal);

bool Operand ()
{
    if (!pCurrent || (pCurrent->type!=lVar && pCurrent->type!=lConst))
    {
        Error("Ожидается переменная или константа",pCurrent->posBegin);
        return false;
    }
    if (pCurrent->type==lVar) WriteVar(pCurrent->index);
    else WriteConst(pCurrent->index);
    pCurrent=pCurrent->prev;
    return true;
}

bool Arifm ()
{
    bool IsAfExist=false;
    if (!Operand()) return false;
    while (pCurrent && pCurrent->type==lAo)
    {
        ECmd cmd;
        switch (pCurrent->relpos)
        {
            case 1:cmd=ADD;break;
            case 2:cmd=SUB;break;
            case 3:cmd=MUL;break;
            case 4:cmd=DIV;break;
        }
        pCurrent=pCurrent->prev;
        if (pCurrent->type==lAfo)
        {
            IsAfExist=true;
            pCurrent=pCurrent->prev;
        }
        if (!Operand()) return false;
        WriteCmd(cmd);
    }
    if (IsAfExist)
    {
		if (pCurrent->type!=lAfc)
		{
			Error("Ожидается закрывающаяся скобка",pCurrent->posBegin);
			return false;
		}
		pCurrent=pCurrent->prev;
	}
    return true;
}

bool Operator ()
{
    if (!pCurrent)
    {
        Error("Ожидается оператор",pCurrent->posBegin);
        return false;
    }
    if (pCurrent->type==lIf)
    {
        if(!IfStatement(true)) return false;
    }
    else if (pCurrent->type==lOutput)
    {
        pCurrent=pCurrent->prev;
        if (!Operand()) return false;
        WriteCmd(OUTPUT);
    }
    else if (pCurrent->type==lInput)
    {
        pCurrent=pCurrent->prev;
        if (pCurrent->type!=lVar)
        {
            Error("Ожидается переменная",pCurrent->posBegin);
            return false;
        }
        WriteVar(pCurrent->index);
        WriteCmd(INPUT);
        pCurrent=pCurrent->prev;
    }
    else if (pCurrent->type==lVar)
    {
        WriteVar(pCurrent->index);
        pCurrent=pCurrent->prev;
        if (pCurrent->type!=lEqual)
        {
            Error("Ожидается = ",pCurrent->posBegin);
            return false;
        }
        pCurrent=pCurrent->prev;
        if (pCurrent) if (!Arifm()) return false;
        WriteCmd(SET);
    }
    else
    {
        Error("Ожидается оператор",pCurrent->posBegin);
        return false;
    }
    return true;
}

bool Operators ()
{
    if (!Operator()) return false;
    while (pCurrent && pCurrent->type==lRaz)
    {
        pCurrent=pCurrent->prev;
        if (!Operator()) return false;
    }
    if (!pCurrent) return true;
    else if (pCurrent->type==lVar || pCurrent->type==lIf || pCurrent->type==lInput || pCurrent->type==lOutput)
    {
        Error("Ожидается разделитель",pCurrent->posBegin);
        return false;
    }
    return true;
}

bool Cmp ()
{
    if (!Operand()) return false;
    if (pCurrent && (pCurrent->type==lRel || pCurrent->type==lEqual || pCurrent->type==lUnequal))
    {
        ECmd cmd;
        switch (pCurrent->relpos)
        {
            case 6: cmd=CMPE;break;
            case 5: cmd=CMPNE;break;
            case 8: cmd=CMPG;break;
            case 7: cmd=CMPL;break;
        }
        pCurrent=pCurrent->prev;
        if (!Operand()) return false;
        WriteCmd(cmd);
    }
    return true;
}

bool LogExpr ()
{
    if (!Cmp()) return false;
    while (pCurrent && pCurrent->type==lAnd)
    {
        pCurrent=pCurrent->prev;
        if (!Cmp()) return false;
        WriteCmd(AND);
    }
    return true;
}

bool Condition ()
{
    if (!LogExpr()) return false;
    while (pCurrent && pCurrent->type==lOr)
    {
        pCurrent=pCurrent->prev;
        if (!LogExpr()) return false;
        WriteCmd(OR);
    }
    return true;
}

int indJmp;

bool IfExpr ()
{
    if (!Condition()) return false;
    if (pCurrent->type!=lThen)
    {
        Error("Ожидается then",pCurrent->posBegin);
        return false;
    }
    int indJz=WriteCmdPtr(-1);
    WriteCmd(JZ);
    if (pCurrent->prev) pCurrent=pCurrent->prev;
    if (!Operators())return false;
    indJmp=WriteCmdPtr(-2);
    int indLast=WriteCmd(JMP);
    SetCmdPtr(indJz,indLast+1);
    return true;
}

bool ElseIfExpr ()
{
    if (pCurrent->type==lEnd) return true;
    if (pCurrent->type!=lElse && pCurrent->type!=lElseif)
    {
        Error("Ожидается else или elseif",pCurrent->posBegin);
        return false;
    }
    if (pCurrent->type==lElse)
    {
        pCurrent=pCurrent->prev;
        if (!Operators()) return false;
    }
    else
    {
        pCurrent=pCurrent->prev;
        int indJmp1=indJmp;
        if (!IfExpr()) return false;
        if (!ElseIfExpr()) return false;
        SetCmdPtr(indJmp1,PostFixSize);
    }
    return true;
}

bool IfStatement (bool internal=false)
{
    if (!pCurrent || pCurrent->type!=lIf)
    {
        Error("Ожидается if", pCurrent->posBegin);
        return false;
    }
    pCurrent=pCurrent->prev;
    if (!IfExpr())return false;
    if (!ElseIfExpr())
    {
        if (!pCurrent || pCurrent->type!=lEnd)
        {
            Error("Ожидается end",pCurrent->posBegin);
            return false;
        }
    }
    if (pCurrent && pCurrent->type!=lEnd && pCurrent->prev!=NULL) pCurrent=pCurrent->prev;
    if (!pCurrent || pCurrent->type!=lEnd)
    {
        Error("Ожидается end",pCurrent->posBegin);
        return false;
    }
    if (!internal)
    {
        if (pCurrent->prev)
        {
            Error("Лишние символы",pCurrent->prev->posBegin);
            return false;
        }
    }
    if (pCurrent->prev) pCurrent=pCurrent->prev;
    SetCmdPtr(indJmp,PostFixSize);
    return true;
}

void GetString (char* str)
{
    char c;
    while ((c=getchar())!=EOF)
    {
        *str=c;
        str++;
    }
    *str=0;
}

struct Stack
{
    EEntryType type;
    int index,count,number;
    Stack *next,*prev;
}*pStackCurrent=NULL,*pStackFirst=NULL,*pStackLast=NULL;

int PopVal()
{
    int retValue;
    if (pStackLast->type==etConst || pStackLast->type==etVar)
    {
        retValue=pStackLast->count;
    }
    else retValue=pStackLast->index;
    if (pStackFirst==pStackLast)
    {
        if (pStackLast) delete pStackLast;
        pStackCurrent=pStackFirst=pStackLast=NULL;
    }
    else
    {
        pStackLast=pStackLast->next;
        if (pStackLast && pStackLast->prev)
        {
            delete pStackLast->prev;
            pStackLast->prev=NULL;
        }
    }
    return retValue;
}

void PushVal(int val)
{
    Stack *p=new Stack;
    p->next=pStackLast;
    if (pStackLast) pStackLast->prev=p;
    if (!pStackFirst) pStackFirst=pStackLast;
    pStackLast=p;
    p->count=val;
    p->type=etConst;
}

void PushElem(PostFixEntry *elem)
{
    Stack *p=new Stack;
    p->next=pStackLast;
    if (pStackLast) pStackLast->prev=p;
    if (!pStackFirst) pStackFirst=pStackLast;
    pStackLast=p;
    p->index=elem->index;
    p->type=elem->type;
    switch (p->type)
    {
       case etVar:
        {
            Var *pcur=FindVarIndex(p->index);
            p->count=pcur->count;
        };break;
       case etConst:
        {
            ConstList *pcur=FindConstIndex(p->index);
            p->count=pcur->count;
        };break;
        default:break;
    }
}

void SetVarAndPop(int val)
{
    FindVarIndex(pStackLast->index)->count=val;
    PopVal();
}

void DeleteStack(Stack *head)
{

    if (head)
    {
        Stack *p;
        while (head)
        {
            p=head;
            head=head->prev;
            delete p;
        }
        pStackCurrent=pStackFirst=pStackLast=NULL;
    }
}

PostFixEntry *Jump(int pos)
{
    PostFixEntry *p=pPostFixFirst;
    int num=0;
    while (p && num!=pos)
    {
        p=p->prev;
        num++;
    }
    return p;
}

void Interpretator()
{
    PostFixEntry *tmp;
    pPostFixCurrent=pPostFixFirst;
    while(pPostFixCurrent)
    {
        if (pPostFixCurrent->type==etCmd)
        {
            switch(pPostFixCurrent->index)
            {
                case JMP:
                {
                    pPostFixCurrent=Jump(PopVal());
                };break;
                case JZ:
                {
                    tmp=Jump(PopVal());
                    bool cond=PopVal();
                    if (cond) pPostFixCurrent=pPostFixCurrent->prev;
                    else pPostFixCurrent=tmp;
                };break;
                case SET:
                {
                    SetVarAndPop(PopVal());
                    pPostFixCurrent=pPostFixCurrent->prev;
                };break;
                case ADD:
                {
                    PushVal(PopVal()+PopVal());
                    pPostFixCurrent=pPostFixCurrent->prev;
                };break;
                case SUB:
                {
                    int x=PopVal();
                    int y=PopVal();
                    PushVal(y-x);
                    pPostFixCurrent=pPostFixCurrent->prev;
                }break;
                case MUL:
                {
                    PushVal(PopVal()*PopVal());
                    pPostFixCurrent=pPostFixCurrent->prev;
                }break;
                case DIV:
                {
                    int x=PopVal();
                    int y=PopVal();
                    PushVal(y/x);
                    pPostFixCurrent=pPostFixCurrent->prev;
                }break;
                case CMPNE:
                {
                    PushVal(PopVal()!=PopVal());
                    pPostFixCurrent=pPostFixCurrent->prev;
                };break;
                case CMPE:
                {
                    PushVal(PopVal()==PopVal());
                    pPostFixCurrent=pPostFixCurrent->prev;
                }break;
                case CMPL:
                {
                    if (PopVal()>PopVal()) PushVal(1);
                    else PushVal(0);
                    pPostFixCurrent=pPostFixCurrent->prev;
                };break;
                case CMPG:
                {
                    if (PopVal()<PopVal()) PushVal(1);
                    else PushVal(0);
                    pPostFixCurrent=pPostFixCurrent->prev;
                };break;
                case AND:
                {
                    if (PopVal() && PopVal()) PushVal(1);
                    else PushVal(0);
                    pPostFixCurrent=pPostFixCurrent->prev;
                };break;
                case OR:
                {
                    if (PopVal() || PopVal()) PushVal(1);
                    else PushVal(0);
                    pPostFixCurrent=pPostFixCurrent->prev;
                };break;
                case INPUT:
                {
                    Var *p=FindVarIndex(pPostFixCurrent->next->index);
                    cout << "Введите значение переменной ";
                    PrintVar(p->begin,p->end);
                    cout << endl;
                    cin >> p->count;
                    pPostFixCurrent=pPostFixCurrent->prev;
                };break;
                case OUTPUT:
                {
                    if (pPostFixCurrent->next->type==etVar)
                    {

                        Var *p=FindVarIndex(pPostFixCurrent->next->index);
                        cout << "Значение переменной ";
                        PrintVar(p->begin,p->end);
                        cout << "=" << p->count << endl;
                        pPostFixCurrent=pPostFixCurrent->prev;
                    }
                    if (pPostFixCurrent->next->type==etConst)
                    {
                        cout << "Константа " << PopVal() << endl;
                        pPostFixCurrent=pPostFixCurrent->prev;
                    }
                };break;
            }
        }
        else
        {
            PushElem(pPostFixCurrent);
            pPostFixCurrent=pPostFixCurrent->prev;
        }
    }
}

int main()
{
    char inputString[1000000];
    cout << "Введите нужный входной поток:" << endl;
    GetString(inputString);
    LexAnalys(inputString);
    OutputLex(pLexListFirst);
    pCurrent=pLexListFirst;
    if (IfStatement()) cout << "Введена корректная конструкция." << endl;
    OutputPostfix(pPostFixFirst);
    cout << "begin\n";
    Interpretator();
    cout << "end\n";
    OutputVar(pVarFirst);
    OutputConst(pConstFirst);

    DeleteLex(pLexListFirst);
    DeleteConstList(pConstFirst);
    DeleteVar(pVarFirst);
    DeletePostFix(pPostFixFirst);
    DeleteStack(pStackFirst);

    return 0;
}
