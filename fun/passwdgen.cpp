#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <list>

using namespace std;

class Generator
{
    list <char> Pass;
    unsigned long long PassSize;
public:
    Generator(unsigned long long psize): PassSize(psize){}
    void PassGenerate()
    {
        srand(time(NULL));
        int a,b;
        for (unsigned long long i=0; i<PassSize; i++)
        {
            a=0+rand()%3;
            switch(a)
            {
                case 0:
                    {
                        b=0+rand()%10 + 48;
                        Pass.push_back(b);
                    };break;
                case 1:
                    {
                        b=0+rand()%26 + 97;
                        Pass.push_back(b);
                    };break;
                case 2:
                    {
                        b=0+rand()%26 + 65;
                        Pass.push_back(b);
                    };break;
                default:break;
            }
        }
    }
    void PassOutput()
    {
        list <char>::iterator it;
        for (it=Pass.begin(); it!=Pass.end(); it++) printf("%c",*it);
        printf("\n");
    }
    void PassSave()
    {
        printf("Do you want to save it?(y/n)");
        char save;
        scanf ("\n%c",&save);
        if (save=='y')
        {
            printf("Enter file name:");
            char name[200];
            int i=0;
            getchar();
            while((name[i]=getchar())!='\n' && (name[i]!=EOF)) i++;
            name[i]=0;
            FILE *f;
            f=fopen(name,"w");
            list <char>::iterator it;
            for (it=Pass.begin(); it!=Pass.end(); it++) fprintf(f,"%c",*it);
            fclose(f);
        }
    }
};

void menu()
{
    printf("Enter length of pass: ");
    unsigned long long n;
    scanf("%Ld",&n);
    Generator psswd(n);
    psswd.PassGenerate();
    printf("Here it is:\n");
    psswd.PassOutput();
    psswd.PassSave();
}

int main()
{
    char start='y';
    do
        {
            menu();
            printf("Do you want to generate another one?(y/n):");
            scanf("\n%c",&start);
        }
    while (start=='y');
    return 0;
}
