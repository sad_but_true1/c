#include <stdio.h>

template <class T>
class Matrix{
	int n,m;
	T* ar;
public:
	Matrix(int n = 0, int m = 0){
		this->n = n;
		this->m = m;
		if ((m == 0) && (n  == 0)) ar = NULL;
		else ar = new T[n*m];
	}
	~Matrix(){
		delete [] ar;
	}
	T& operator () (int i, int j){
		return ar[i*m+j];
	}
	Matrix& operator = ( Matrix& copy){
		n = copy.n;
		m = copy.m;
		if (!ar) delete [] ar;
		ar = new T[n*m];
		for (int i = 0; i<n; i++)
			for (int j = 0; j<m; j++)
				ar[i*m+j] = copy(i,j);
		return *this;
	}
	/*Matrix& operator * (){
		
	}*/
	int line_size(){
		return n;
	}
	int column_size(){
		return m;
	}
};

int main(){
	Matrix <int> mtrx(5,5);
	for (int i=0; i<mtrx.line_size(); i++)
		for (int j=0; j<mtrx.column_size(); j++)
			scanf("%d",&mtrx(i,j));
	for (int i=0; i<mtrx.line_size(); i++){
		for (int j=0; j<mtrx.column_size(); j++)
			printf("%d ",mtrx(i,j));
		printf("\n");
	}
	Matrix <int> p;
	p = mtrx;
	printf("%d",  p(1,1));
	return 0;
}
